package lesson3.newTasks.objectMethods;

import java.util.Calendar;

public class Professor extends Doctor {
    private int courseCount;

    public Professor(String name, Calendar scientificDegreeDate, int scientificDegreeNumber, int courseCount) {
        super(name, scientificDegreeDate, scientificDegreeNumber);
        this.courseCount = courseCount;
    }

    public int getCourseCount() {
        return courseCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        if (!super.equals(o)) return false;
        Professor professor = (Professor) o;
        return courseCount == professor.courseCount;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 17 * courseCount;
    }

    @Override
    public String toString() {
        return super.toString() + ", прочитано курсов: " + courseCount;
    }
}
