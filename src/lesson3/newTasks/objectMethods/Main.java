package lesson3.newTasks.objectMethods;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
/*
Напишите класс Doctor определите поля для имени, даты получения ученой степени и числа научных статей.
Определите метод для увеличения числа научных статей.
Напишите класс Professor сделайте его наследником класса Doctor. Определите в нем поле для числа прочитанных курсов.
Напишите приложение демонстрирующее работу методов.
*/
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Doctor doctor = new Doctor("Заур Алисултанов", new GregorianCalendar(2016, Calendar.MAY, 23), 2);
        System.out.println("Доктор: " + doctor.getName() + ", даты получения ученой степени: "
                + formatter.format(doctor.getScientificDegreeDate().getTime())
                + ", научных статей: " + doctor.getScientificDegreeCount());
        doctor.raiseScientificDegreeCount(2);
        System.out.println("Доктор: " + doctor.getName() + ", даты получения ученой степени: "
                + formatter.format(doctor.getScientificDegreeDate().getTime())
                + ", научных статей: " + doctor.getScientificDegreeCount());

        Professor professor = new Professor("Глинских Вячеслав Николаевич", new GregorianCalendar(2018, Calendar.MARCH, 18), 90, 10);
        System.out.println("Профессор: " + professor.getName() + ", даты получения ученой степени: "
                + formatter.format(professor.getScientificDegreeDate().getTime())
                + ", научных статей: " + professor.getScientificDegreeCount() + ", прочитанных курсов " + professor.getCourseCount());
        professor.raiseScientificDegreeCount(2);
        System.out.println("Профессор: " + professor.getName() + ", даты получения ученой степени: "
                + formatter.format(professor.getScientificDegreeDate().getTime())
                + ", научных статей: " + professor.getScientificDegreeCount() + ", прочитанных курсов " + professor.getCourseCount());

        Doctor doctor1 = new Professor("Глинских Вячеслав Николаевич", new GregorianCalendar(2018, Calendar.MARCH, 18), 92, 10);
        System.out.println(doctor1.getName() + " - " + doctor1.getClass().getSimpleName());

/*
Определите в классе Doctor методы equals и hashcode. Определите в классе Professor методы equals и hashcode используя методы equals и hashcode для базового класса Doctor.
Напишите приложение демонстрирующее работу методов.
*/
        System.out.println();
        System.out.println("Hashcode doctor: " + doctor.hashCode());
        System.out.println("Hashcode professor: " + professor.hashCode());
        System.out.println("Hashcode doctor1: " + professor.hashCode());
        System.out.println("doctor и professor разные люди:" + !doctor.equals(professor));
        System.out.println("professor и doctor1 разные люди:" + !professor.equals(doctor1));
/*
Определите в классе Doctor метод toString. Определите в классе Professor метод toString используя методы toString для
базового класса Doctor. Напишите приложение демонстрирующее работу метода.
*/
        System.out.println("\n Метод toString. ");
        System.out.println(doctor);
        System.out.println(professor);
        System.out.println(doctor1);

/*
Определите метод для клонирования объекта Doctor. Напишите приложение демонстрирующее работу метода.
*/
        System.out.print("\n Клонирование объекта Doctor: ");
        try {
            Doctor doctor2 = doctor.clone();
            System.out.println(doctor2);
            doctor2.raiseScientificDegreeCount(1);
            System.out.println("объекты после клонирования и изменения клона:");
            System.out.println("doctor : " + doctor);
            System.out.println("clone  : " + doctor2);

        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
    }
}


