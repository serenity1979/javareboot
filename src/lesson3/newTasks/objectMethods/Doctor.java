package lesson3.newTasks.objectMethods;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Doctor implements Cloneable {
    private String name;
    private Calendar scientificDegreeDate;
    private int scientificDegreeCount;

    public Doctor(String name, Calendar scientificDegreeDate, int scientificDegreeNumber) {
        this.name = name;
        this.scientificDegreeDate = scientificDegreeDate;
        this.scientificDegreeCount = scientificDegreeNumber;
    }

    public void raiseScientificDegreeCount(int newScientificDegreeCount) {
        this.scientificDegreeCount += newScientificDegreeCount;
    }

    public String getName() {
        return name;
    }

    public Calendar getScientificDegreeDate() {
        return (Calendar) scientificDegreeDate.clone();
    }

    public int getScientificDegreeCount() {
        return scientificDegreeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        Doctor doctor = (Doctor) o;

        if (name == null) {
            if (doctor.name != null)
                return false;
        } else if (!name.equals(doctor.name))
            return false;

        if (scientificDegreeDate == null) {
            if (doctor.scientificDegreeDate != null)
                return false;
        } else if (!scientificDegreeDate.equals(doctor.scientificDegreeDate))
            return false;

        return scientificDegreeCount == doctor.scientificDegreeCount;
    }

    @Override
    public int hashCode() {
        return 7 * name.hashCode() + 11 * scientificDegreeCount + 13 * scientificDegreeDate.hashCode();

    }

    @Override
    public String toString() {

        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

        return name + ", даты получения ученой степени:" + formatter.format(scientificDegreeDate.getTime()) + ", научных статей:" + scientificDegreeCount;

    }

    @Override
    public Doctor clone() throws CloneNotSupportedException {

        Doctor cloned = (Doctor) super.clone();
        cloned.scientificDegreeDate = (Calendar) scientificDegreeDate.clone();
        return cloned;
    }
}
