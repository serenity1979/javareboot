package lesson3.newTasks.classesAndObjects;

/*
Напишите класс ConstructorStudent c полями экземпляра для имени и для количества пройденных курсов. Сделайте все поля приватными.
Для задания имени и количества пройденных курсов создайте конструктор.
*/


public class ConstructorStudent {
    private String name;
    private int courseCount;

    public ConstructorStudent(String name, int courseCount) {
        this.name = name;
        this.courseCount = courseCount;
    }

    public String getName() {
        return name;
    }

    public int getCourseCount() {
        return courseCount;
    }
}
