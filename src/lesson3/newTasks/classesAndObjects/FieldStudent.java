package lesson3.newTasks.classesAndObjects;

/*
Напишите класс FieldStudent c полями экземпляра для имени и для порядкового номера и статическим полем для следующего свободного номера.
Первый свободный номер должен быть 1.

*/
public class FieldStudent {
    public String name;
    public int studentID;
    public static int nextStudentID = 1;

}
