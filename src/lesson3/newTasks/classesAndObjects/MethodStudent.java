package lesson3.newTasks.classesAndObjects;
/*
Напишите класс MethodStudent c полями экземпляра для имени и для порядкового номера и статическим полем для следующего свободного номера. Первый свободный номер должен быть 1.
Сделайте все поля приватными. Для задания имени создайте метод setName для задания порядкового номера разработайте метод setStudentId.
 */

public class MethodStudent {
    private String name;
    private int studentID;
    private static int nextStudentID = 1;

    public void setName(String name) {
        this.name = name;
    }

    public void setStudentID() {
        this.studentID = nextStudentID;
        nextStudentID++;
    }

    public String getName() {
        return name;
    }

    public int getStudentID() {
        return studentID;
    }

}
