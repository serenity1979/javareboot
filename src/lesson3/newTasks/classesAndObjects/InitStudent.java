package lesson3.newTasks.classesAndObjects;

/*
Напишите класс InitStudent c полями экземпляра для имени и для порядкового номера и статическим полем для следующего свободного номера.
Сделайте все поля приватными. Для задания имени создайте метод setName.
Создайте два блока инициализации статический и экземпляра. Статический блок инициализации должен инициализировать статическое поле случайным значением от 0 до 10000.
Блок инициализации экземпляра должен инициализировать поле для порядкого номера значением следующего свободного номера.

      Random generator = new Random();
      nextId = generator.nextInt(10000);
*/
import java.util.Random;

public class InitStudent {
    private String name;
    private int studentID;
    private static int nextStudentID;

    static {
        Random generator = new Random();
        nextStudentID = generator.nextInt(10000);
    }
    {
        studentID = nextStudentID;
        nextStudentID++;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getStudentID() {
        return studentID;
    }
}
