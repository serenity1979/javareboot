package lesson3.newTasks.classesAndObjects;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

/*
Напишите приложение, которое берет массив с именами студентов и создаёт объекты FieldStudent, присваивая им имена и порядковые номера, и сохраняет ссылки на объекты в массив.
После чего приложение проходит по массиву объектов Student циклом foreach и выводит для каждого имя и порядковый номер.
*/
        String[] studentNameArray = {"Иванов И.И.", "Петров П.П.", "Сидоров С.С.", "Александрова А.А.", "Зинкина З.З"};
        int length = studentNameArray.length;

        System.out.println(" класс FieldStudent ");
        FieldStudent[] fieldStudents = new FieldStudent[length];
        for (int i = 0; i < length; i++) {
            FieldStudent student = new FieldStudent();
            student.name = studentNameArray[i];
            student.studentID = FieldStudent.nextStudentID;
            FieldStudent.nextStudentID += 1;
            fieldStudents[i] = student;
        }
        for (FieldStudent student : fieldStudents) {
            System.out.println("Студент № " + student.studentID + " - " + student.name);
        }
        System.out.println();

/*
Напишите приложение которое берет массив с именами студентов и создаёт объекты MethodStudent присваивая им имена и порядковые номера и сохраняет ссылки на объекты в массив.
После чего приложение проходит по массиву объектов MethodStudent циклом foreach и выводит для каждого имя и порядковый номер.
*/
        System.out.println(" класс MethodStudent ");
        MethodStudent[] methodStudents = new MethodStudent[length];
        for (int i = 0; i < length; i++) {
            MethodStudent student = new MethodStudent();
            student.setName(studentNameArray[i]);
            student.setStudentID();
            methodStudents[i] = student;
        }
        for (MethodStudent student : methodStudents) {
            System.out.println("Студент № " + student.getStudentID() + " - " + student.getName());
        }
        System.out.println();

/*
Напишите приложение которое берет массив с именами студентов и создаёт объекты InitStudent присваивая им имена и сохраняет ссылки на объекты в массив.
После чего приложение проходит по массиву объектов InitStudent циклом foreach и выводит для каждого имя и порядковый номер.
*/
        System.out.println(" класс InitStudent ");
        InitStudent[] initStudents = new InitStudent[length];
        for (int i = 0; i < length; i++) {
            initStudents[i] = new InitStudent();
            initStudents[i].setName(studentNameArray[i]);
        }
        for (InitStudent student : initStudents) {
            System.out.println("Студент № " + student.getStudentID() + " - " + student.getName());
        }
        System.out.println();

/*
Напишите приложение которое создаёт объекты ConstructorStudent передавая в конструктор имена и числа пройденных курсов.
Затем приложение проходит по массиву объектов ConstructorStudent циклом foreach и выводит для каждого имя и число пройденных курсов.
*/
        System.out.println(" класс ConstructorStudent ");
        ConstructorStudent[] constructorStudents = new ConstructorStudent[length];
        Random generator = new Random();
        for (int i = 0; i < length; i++) {
            constructorStudents[i] = new ConstructorStudent(studentNameArray[i],generator.nextInt(8));
        }
        for (ConstructorStudent student : constructorStudents) {
            System.out.println("Студент:" + student.getName() + ", пройдено курсов:  " + student.getCourseCount());
        }
        System.out.println();
/*
Напишите приложение, которое создаёт объекты ManyConstructorStudent используя различные конструкторы.
Затем приложение проходит по массиву объектов ManyConstructorStudent циклом foreach и выводит для каждого имя, порядковый номер и средний бал.
*/
        System.out.println(" класс ManyConstructorStudent ");
        ManyConstructorStudent[] manyConstructorStudents = new ManyConstructorStudent[4];

        manyConstructorStudents[0] = new ManyConstructorStudent();
        manyConstructorStudents[1] = new ManyConstructorStudent(90.6);
        manyConstructorStudents[2] = new ManyConstructorStudent("Иванов И.И.",98.2);
        manyConstructorStudents[3] = new ManyConstructorStudent("Петров П.П.", 80.9);

        for (ManyConstructorStudent student : manyConstructorStudents) {
            System.out.println(student);
        }
    }
}
