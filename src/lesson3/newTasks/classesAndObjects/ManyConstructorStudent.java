package lesson3.newTasks.classesAndObjects;
/*
Напишите класс ManyConstructorStudent c полями экземпляра для имени, порядкового номера и среднего бала (double gpa).
Добавьте статическое поле для следующего свободного порядкового номера. Сделайте все поля приватными.
Добавьте блок инициализации экземпляра для задания порядкового номера студента.

Создайте три конструктора.
 Первый принимает имя и средний бал.
 Второй принимает только средний бал. Имя получается как "Student #" + nextId.
 Третий конструктор не принимает параметров. Третий конструктор вызывает второй конструктор. Второй конструктор вызывает первый конструктор.
*/
public class ManyConstructorStudent {
    private String name;
    private int studentID;
    private double gpa;
    private static int nextStudentID = 1;

    {
        studentID = nextStudentID;
        nextStudentID++;
    }

    public ManyConstructorStudent(String name, double gpa) {
        this.name = name;
        this.gpa = gpa;
    }

    public ManyConstructorStudent(double gpa) {
         this("Student #" + nextStudentID, gpa);
    }

    public ManyConstructorStudent() {
        this(0);
    }

    @Override
    public String toString() {
        return "Студент № " + this.studentID + " - " + this.name + ", средний балл=" + this.gpa;
    }
}
