package lesson3.newTasks.inheritance;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
/*
Напишите приложение демонстрирующее работу методов.
*/
        GregorianCalendar calendarForDoctor = new GregorianCalendar(2019, Calendar.NOVEMBER, 20);
        Doctor doctor = new Doctor("Авазовский А.В.", calendarForDoctor.getTime(), 150);
        System.out.println(doctor);
        doctor.raiseScientificDegreeCount(5);
        System.out.println(doctor.getName() + " написано научных статей: " + doctor.getScientificDegreeCount() + System.lineSeparator());

        GregorianCalendar calendarForProfessor = new GregorianCalendar(2017, Calendar.APRIL, 14);
        Professor professor = new Professor("Мудрый А.П.", calendarForProfessor.getTime(), 400, 8);
        System.out.println(professor);
        professor.raiseScientificDegreeCount(6);
        System.out.println(professor.getName() + " написано научных статей: " + professor.getScientificDegreeCount() + System.lineSeparator());

        GregorianCalendar calendarForDean = new GregorianCalendar(2015, Calendar.SEPTEMBER, 30);
        Dean dean = new Dean("Дивный К.М.", calendarForDean.getTime(), 500, 10, 12);
        System.out.println(dean);

/*
Создайте объект класса Dean. Продемонстрируйте работу методов.
Проведите восходящее преобразование к типу Professor. Продемонстрируйте работу доступных методов. Проверьте что тип объекта Dean.
Проведите восходящее преобразование к типу Doctor. Продемонстрируйте работу доступных методов. Проверьте что тип объекта Dean.
 */

        Professor professor1 = dean;
        System.out.println(professor1);
        System.out.println("класс Dean: " + (professor1 instanceof Dean));

        Doctor doctor1 = professor1;
        System.out.println(doctor1);
        System.out.println("класс Dean: " + (doctor1 instanceof Dean));

/*
Проведите нисходящее преобразование к типу Professor. Продемонстрируйте работу доступных методов.
Проведите нисходящее преобразование к типу Dean. Продемонстрируйте работу доступных методов.
 */
        GregorianCalendar calendarForDean2 = new GregorianCalendar(2010, Calendar.JULY, 10);
        Doctor doctor2 = new Dean("Тестов Н.П.", calendarForDean2.getTime(), 500, 20, 15);
        System.out.println(doctor2);
        System.out.println("Преобразовать в класс Professor: " + (doctor2 instanceof Professor));
        Professor professor2 = (Professor) doctor2;
        System.out.println(professor2);
        System.out.println("Преобразовать в класс Dean: " + (professor2 instanceof Dean));
        Dean dean1 = (Dean) professor2;
        System.out.println(dean1);

    }
}
