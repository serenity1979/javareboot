package lesson3.newTasks.inheritance;

import java.util.Date;

/*
Напишите класс Doctor определите поля для имени, даты получения ученой степени и числа научных статей. Определите метод для увеличения числа научных статей.
 */
public class Doctor {
    private String name;
    private Date scientificDegreeDate;
    private int scientificDegreeCount;

    public Doctor(String name, Date scientificDegreeDate, int scientificDegreeCount) {
        this.name = name;
        this.scientificDegreeDate = scientificDegreeDate;
        this.scientificDegreeCount = scientificDegreeCount;
    }

    public String getName() {
        return name;
    }

    public Date getScientificDegreeDate() {
        return (Date) scientificDegreeDate.clone();
    }

    public int getScientificDegreeCount() {
        return scientificDegreeCount;
    }

    public void raiseScientificDegreeCount(int newScientificDegreeCount) {
        this.scientificDegreeCount += newScientificDegreeCount;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + name + ", даты получения ученой степени " + scientificDegreeDate + ", написано научных статей:" + scientificDegreeCount + ". ";
    }
}
