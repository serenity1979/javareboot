package lesson3.newTasks.inheritance;

import java.util.Date;

/*
Напишите класс Professor сделайте его наследником класса Doctor. Определите в нем поле для числа прочитанных курсов.
 */
public class Professor extends Doctor {
    private int courseCount;

    public Professor(String name, Date scientificDegreeDate, int scientificDegreeCount, int courseCount) {
        super(name, scientificDegreeDate, scientificDegreeCount);
        this.courseCount = courseCount;
    }

    public int getCourseCount() {
        return courseCount;
    }

    @Override
    public String toString() {
        return super.toString() + "Прочитанных курсов " + courseCount + ".";
    }
}
