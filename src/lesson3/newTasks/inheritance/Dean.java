package lesson3.newTasks.inheritance;

/*
Напишите класс Dean сделайте его наследником класса Professor. Определите в нем поле для числа профессоров работающих на факультете.
 */

import java.util.Date;

public class Dean extends Professor {
    int professorCount;

    public Dean(String name, Date scientificDegreeDate, int scientificDegreeCount, int courseCount, int professorCount) {
        super(name, scientificDegreeDate, scientificDegreeCount, courseCount);
        this.professorCount = professorCount;
    }

    public int getProfessorCount() {
        return professorCount;
    }

    @Override
    public String toString() {
        return super.toString() + " Профессоров на факультете: " + professorCount + ". ";
    }
}
