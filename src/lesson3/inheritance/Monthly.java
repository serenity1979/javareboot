package lesson3.inheritance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Monthly extends Appointment {
    //.. ежемесячная встреча

    public Monthly(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);
    }

    @Override
    public boolean occursOn(int year, int month, int day) throws ParseException {

        boolean onDay = super.occursOn(year, month, day);

        if (onDay) {
            return true;
        } else {
            DateFormat dateFormatter = new SimpleDateFormat(String.format("yyyy-MM-dd"));
            Date toDay = dateFormatter.parse(String.format("%d-%d-%d", year, month, day));

            if (getDate().getDate() == day && toDay.after(getDate())) {
                return true;
            } else return false;
        }
    }
}
