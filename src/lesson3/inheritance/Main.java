package lesson3.inheritance;

import java.text.ParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Appointment[] appointments = new Appointment[5];
        try {
            appointments[0] = new Daily("Прием витамин", 2019, 11, 15);
            appointments[1] = new Monthly("Встреча с друзьями", 2019, 9, 10);
            appointments[2] = new Onetime("Запланировать отпуск", 2019, 11, 12);
            appointments[3] = new Monthly("Прием у врача", 2019, 10, 15);
            appointments[4] = new Daily("Полить цветы", 2019, 8, 5);
        } catch (ParseException e) {
            System.out.println("Не удалось создать массив встреч, дата не корректна");
        }

        int year, month, day;

        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Введите дату в следующем порядке: год, месяц, день");
            year = in.nextInt();
            month = in.nextInt();
            day = in.nextInt();
            for (Appointment meeting : appointments) {
                if (meeting.occursOn(year, month, day)) {
                    meeting.print();
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
