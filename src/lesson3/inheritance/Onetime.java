package lesson3.inheritance;

import java.text.ParseException;


public class Onetime extends Appointment {
   //.. разовая встреча

    public Onetime(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);
    }

}
