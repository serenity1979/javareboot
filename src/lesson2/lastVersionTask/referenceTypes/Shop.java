package lesson2.lastVersionTask.referenceTypes;

public class Shop {
    private String name;

    public Shop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void pantCar(Car car, String color) {
        car.pantCar(color);
    }
}
