package lesson2.lastVersionTask.referenceTypes;
/*
Напишите класс Car. Добавьте поле для цвета машины и метод для покраски машины.
Напишите класс Shop. Добавьте метод для покраски машины.
Напишите класс Owner определите методы для задания и получения машины.
Напишите программу, которая задаёт одну машину двум владельцам. Получите машину одного владельца и покрасьте её используя класс Shop.
Возьмите машину другого владельца и распечатайте ее цвет.
*/

public class Main {
    public static void main(String[] args) {
        Car car = new Car("BMW", "белый");
        Owner owner1 = new Owner("Иванов И.И.", car);
        Owner owner2 = new Owner("Петров П.П.");
        owner2.setCar(car);

        System.out.println("======== При покупке ========");
        System.out.println(owner1);
        System.out.println(owner2);

        Shop.pantCar(owner1.getCar(), "синий");
        System.out.println("\n====== После покраски ======");
        System.out.println(owner1);
        System.out.println(owner2);
    }
}
