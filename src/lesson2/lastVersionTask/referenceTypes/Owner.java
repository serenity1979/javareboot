package lesson2.lastVersionTask.referenceTypes;

public class Owner {
    private String name;
    private Car car;

    public Owner(String name) {
        this.name = name;
    }

    public Owner(String name, Car car) {
        this.name = name;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Владелец: " + name +  ", " + car;
    }
}
