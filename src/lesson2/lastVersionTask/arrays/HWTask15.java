package lesson2.lastVersionTask.arrays;
/*Напишите приложение которые инициализует значения массива размера 10 используя консольный ввод.
После чего выводит четыре строки содержащие
•    Все элементы по четным индексам.
•    Все четные элементы.
•    Все элементы в обратном порядке.
•    Только первый и последний элементы.
*/

import java.util.Scanner;

public class HWTask15 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d элемент массива: ", i);
            array[i] = input.nextInt();
        }
        input.close();

        evenIndexElements(array);
        everyEvenElement(array);
        allElementsInReverseOrder(array);
        firstAndLastElement(array);
    }

    public static void evenIndexElements(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i = i + 2) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }

    public static void everyEvenElement(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println("]");
    }

    public static void allElementsInReverseOrder(int[] array) {
        System.out.print("[");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }

    public static void firstAndLastElement(int[] array) {
        System.out.println(array[0] + " и " + array[array.length - 1]);
    }
}
