package lesson2.lastVersionTask.arrays;

/*
Напишите метод sumWithoutSmallest, который вычисляет сумму элементов массива кроме наименьшего используя один цикл.
В цикле обновляйте сумму и наименьшее значение. После цикла верните разницу.
 */

public class HWTask16 {
    public static void main(String[] args) {
        int[] myArray = {10, 20, 6, 5, 98};
        System.out.println(sumWithoutSmallest(myArray));
    }

    public static int sumWithoutSmallest(int[] array) {
        int smallest = array[0];
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            if (array[i] < smallest) {
                smallest = array[i];
            }
        }
        return sum - smallest;

    }
}