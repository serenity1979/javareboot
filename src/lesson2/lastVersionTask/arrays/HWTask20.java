package lesson2.lastVersionTask.arrays;
/*
Write a method
public static boolean sameSet(int[] a, int[] b)
that checks whether two arrays have the same elements in some order, ignoring duplicates. For example, the two arrays
1  4  9  16  9  7  4  9  11
and
11  11  7  9  16  4  1
would be considered identical. You will probably need one or more helper methods.
*/

/*
Напишите метод, котрорый проверяет, что два массива содержат те же элементы в любом порядке игнорируя дубликаты
*/

import java.util.Arrays;
import java.util.Scanner;

public class HWTask20 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] firstArray = {1, 4, 9, 16, 9, 7, 4, 9, 11};

        int[] secondArray = {11, 11, 7, 9, 16, 4, 1};

        boolean isSameSets = sameSet(firstArray, secondArray);
        System.out.printf("Это тот же массив?: %s", isSameSets);
    }

    private static boolean sameSet(int[] a, int[] b) {
        if (Arrays.equals(a,b)){
            return true;
        }

        for (int item : a) {
            if (Arrays.stream(b).anyMatch(value -> item == value)) {

            }
            
        }

        return false;
    }
}
