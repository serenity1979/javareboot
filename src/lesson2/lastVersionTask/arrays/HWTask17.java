package lesson2.lastVersionTask.arrays;

/*
Вычислите переменную сумму всех элементов массива. Например если программа считывает
1  4  9  16  9  7  4  9  11
то результатом работы будет
1 – 4 + 9 – 16 + 9 – 7 + 4 – 9 + 11 = –2
*/

import java.util.Scanner;

public class HWTask17 {
    public static void main(String[] args) {
        int alternatingSum = 0;

        System.out.print("Введите кол-во вводимых элементов:");
        Scanner input = new Scanner(System.in);
        int arrayLength = input.nextInt();

        int[] array = new int[arrayLength];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d element of array: ", i);
            array[i] = input.nextInt();
            if (i % 2 == 0) {
                alternatingSum += array[i];
            } else alternatingSum -= array[i];
        }
        input.close();

        System.out.println("alternatingSum=" + alternatingSum);
    }
}
