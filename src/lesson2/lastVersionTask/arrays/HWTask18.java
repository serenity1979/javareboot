package lesson2.lastVersionTask.arrays;
/*
Write a method that reverses the sequence of elements in an array. For example, if you call the method with the array
1  4  9  16  9  7  4  9  11
then the array is changed to
11  9  4  7  9  16  9  4  1
*/

import java.util.Arrays;

public class HWTask18 {
    public static void main(String[] args) {
        int[] array = {1, 4, 9, 16, 9, 7, 4, 9, 11};
        reversesSequence(array);
        System.out.println(Arrays.toString(array));
    }

    public static void reversesSequence(int[] array) {
        int arrayLength = array.length - 1;
        for (int i = 0; i < array.length / 2; i++) {
            int variable = array[i];
            array[i] = array[arrayLength - i];
            array[arrayLength - i] = variable;
        }
    }
}