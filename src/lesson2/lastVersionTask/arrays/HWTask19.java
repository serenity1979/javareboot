package lesson2.lastVersionTask.arrays;

/*
Write a method public static boolean equals(int[] a, int[] b) that checks whether two arrays have the same elements in the same order.
*/

public class HWTask19 {
    public static void main(String[] args) {
        int[] arrayA = {0, 6, 5, 7};
        int[] arrayB = {7, 5, 6, 0};
        int[] arrayC = {0, 6, 5, 7};

        System.out.println(equals(arrayA, arrayB));
        System.out.println(equals(arrayA, arrayC));
    }

    public static boolean equals(int[] a, int[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
}