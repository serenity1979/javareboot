package lesson2.lastVersionTask.primitiveTypes;

import java.util.Scanner;

/*
Напишите приложение которое считывает с консоли число дней и выводит число миллисекунд в этом числе дней.
*/
public class HWTask12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("введите число дней:");
        int days = scanner.nextInt();
        int HOURS_IN_DAY=24;
        int MIN_IN_HOUR = 60;
        int SEC_IN_MIN = 60;
        int MS_IN_SEC=1000;
        int dayMs = days * HOURS_IN_DAY * MIN_IN_HOUR * SEC_IN_MIN+MS_IN_SEC;
        System.out.println(dayMs +" миллисекунд");
    }
}
