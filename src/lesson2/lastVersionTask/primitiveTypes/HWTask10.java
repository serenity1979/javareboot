package lesson2.lastVersionTask.primitiveTypes;

import java.util.Scanner;

public class HWTask10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Число:");
        double number = scanner.nextDouble();

        System.out.printf("квадрат - %.3f, куб - %.3f и четвертая степень - %.3f",
                number*number,
                number*number*number,
                Math.pow(number,4));
    }
}
