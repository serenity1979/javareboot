package lesson2.lastVersionTask.primitiveTypes;

import java.util.Scanner;

/*
Напишите приложение которое считывает с консоли два логических значения и выводит результат выполнения  AND, OR, XOR.
*/
public class HWTask13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("введите логическое значение №1:");
        boolean value1 = scanner.nextBoolean();
        System.out.print("введите логическое значение №2:");
        boolean value2 = scanner.nextBoolean();
        System.out.println("AND: " + (value1 & value2) );
        System.out.println("OR: " + (value1 | value2) );
        System.out.println("XOR: " + (value1 ^ value2) );
    }
}
