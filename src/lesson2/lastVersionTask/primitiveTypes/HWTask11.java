package lesson2.lastVersionTask.primitiveTypes;
/*
 Write a program that prompts the user for two integers and then prints
 •    The sum
 •    The difference
 •    The product
 •    The average
 •    The distance (absolute value of the difference)
 •    The maximum (the larger of the two)
 •    The minimum (the smaller of the two)
 Hint: The max and min functions are declared in the Math class.

 Напишите программу, которая запрашивает у пользователя два целых числа и затем печатает
 • Сумма
 • Разница
 • Произведение
 * Среднее значение
 • Расстояние (абсолютное значение разницы)
 * Максимум (больший из двух)
 * Минимум (меньший из двух)
 Подсказка: функции max и min объявляются в классе Math.
*/

import java.util.Scanner;

public class HWTask11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("введите число:");
        int number1 = scanner.nextInt();
        System.out.print("введите число:");
        int number2 = scanner.nextInt();
        System.out.println("Сумма чисел: " + (number1 + number2));
        System.out.println("Разница чисел: " + (number1 - number2));
        System.out.println("Произведение чисел: " + (number1 * number2));
        System.out.println("Среднее значение чисел: " + (number1 + number2) / 2);
        System.out.println("Расстояние (абсолютное значение разницы): " + Math.abs(number1 - number2));
        System.out.println("Максимум (больший из двух): " + Math.max(number1, number2));
        System.out.println("Минимум (меньший из двух): " + Math.min(number1, number2));
    }
}