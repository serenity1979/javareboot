package lesson2.lastVersionTask.controlFlow;

import java.util.GregorianCalendar;
import java.util.Scanner;

public class HWTask05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите год для анализа:");
        int setYear = scanner.nextInt();

        // условия григорианского календаря
        GregorianCalendar GregorianCalendar = new GregorianCalendar();
        if (GregorianCalendar.isLeapYear(setYear)) {
            System.out.printf("В григорианском календаре %d - високосный год%n", setYear);
        } else {
            System.out.printf("В григорианском календаре %d - не високосный год%n", setYear);
        }

        /* анализ года при помощи опретора if-else
        Григорианский календарь - Год не является високосным, если он не кратен 4, либо он кратен 100, но при этом не кратен 400.
         */

        if (((setYear % 4 != 0) || (setYear % 100 == 0)) && (setYear % 400 != 0)) {
            System.out.printf("В григорианском календаре %d - не високосный год%n", setYear);
        } else {
            System.out.printf("В григорианском календаре %d - високосный год%n", setYear);
        }
    }
}