package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("число a:");
        int a = scanner.nextInt();
        System.out.print("число b:");
        int b = scanner.nextInt();
        System.out.print("число c:");
        int c = scanner.nextInt();

        if (a > b && b > c) {
            System.out.println("убывающий");
        } else if (a < b && b < c) {
            System.out.println("возрастающий");
        } else System.out.println("немонотонная последовательность");

    }
}