package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите размерность таблицы умножения:");
        int factorNumber = scanner.nextInt();

        for (int i = 1; i <= factorNumber; i++) {
            System.out.println();
            for (int j = 1; j <= factorNumber; j++) {
                System.out.printf("%3d ", j * i);
            }
        }
    }
}