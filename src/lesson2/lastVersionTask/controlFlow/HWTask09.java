package lesson2.lastVersionTask.controlFlow;
/*
Write a program that reads an integer and displays, using asterisks, a filled diamond of the given side length.
For example, if the side length is 4, the program should dis­play
Напишите программу, которая читает целое число и отображает, используя звездочки, заполненный ромб с заданной длиной стороны.
Например, если длина стороны равна 4, программа должна отобразить
     *
    ***
   *****
  *******
   *****
    ***
     *
 */

import java.util.Scanner;

public class HWTask09 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("сторона ромба=");
        int side = scanner.nextInt();

        for (int i = 1; i < side; i++) {
            printStars(side, i);
        }
        for (int i = side; i > 0; i--) {
            printStars(side, i);
        }

    }

    public static void printStars(int lengthSide, int countStart) {
        int leftEmptCcount = lengthSide - countStart;
        String str1 = new String(new char[leftEmptCcount]).replace("\0", " ");
        String str2 = new String(new char[countStart * 2 - 1]).replace("\0", "*");
        System.out.println(str1 + str2);
    }
}