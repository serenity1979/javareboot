package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double number = scanner.nextDouble();
        String valueNumber = "";
        if (Math.abs(number) < 1) {
            valueNumber = "маленькое ";
        }
        if (Math.abs(number) > 1000000) {
            valueNumber = "большое ";
        }

        if (number == 0) {
            System.out.println("ноль");
        } else if (number > 0) {
            System.out.println(valueNumber + "положительное");
        } else System.out.println(valueNumber + "отрицательное");
    }
}