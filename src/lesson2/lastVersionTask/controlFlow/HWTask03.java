package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        if (number < 0) {
            number *= -1;
        }
        int i = 1;
        while (number >= Math.pow(10, i)) {
            i++;
        }
        System.out.println(i + "-разрядое число");
    }
}