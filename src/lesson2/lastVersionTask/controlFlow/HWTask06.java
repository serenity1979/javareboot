package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово: ");
        String word = scanner.next();

        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < word.length() - i; j++) {
                System.out.println(word.substring(j, j + i + 1));
            }
        }
    }
}