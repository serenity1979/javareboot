package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите размер квадрата:");
        int k = scanner.nextInt();

        for (int i = 1; i <= k; i++) {
            printSymbol(k);
            System.out.print("  ");
            if (i == 1 || i == k) {
                printSymbol(k);
            } else printFirstLastSymbol(k);
            System.out.println();
        }
    }

    public static void printSymbol(int count) {
        for (int i = 0; i < count; i++) {
            System.out.print("*");
        }
    }

    public static void printFirstLastSymbol(int count) {
        final int THICKNESS_SIDE = 2;
        System.out.print("*");
        for (int i = 1; i <= count - THICKNESS_SIDE; i++) {
            System.out.print(" ");
        }
        System.out.print("*");
    }
}
