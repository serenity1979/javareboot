package lesson2.lastVersionTask.controlFlow;

import java.util.Scanner;

public class HWTask01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        System.out.print("Число является ");
        if (number == 0) {
            System.out.println("нулевым");
        } else if (number > 0) {
            System.out.println("положительным");
        } else System.out.println("отрицательным");
    }
}