package lesson2.lastVersionTask.wrappers;
/*
Напишите приложение показывающее что при упаковке целых чисел используется кеширование оберток для диапазона -128, ... ,127.
*/

public class HWTask14 {
    public static void main(String[] args) {
        Integer[] arrayInteger = new Integer[260];
        for (int i = 0; i < 260; i++) {
            arrayInteger[i] = i - 130;
        }
        for (int i = 0; i < 260; i++) {
            Integer integer = i - 130;
            if (arrayInteger[i] != integer) {
                System.out.println(arrayInteger[i] + " " + integer + " разные ссылки");
            }
        }
    }
}
