package lesson2.oldVersionTask;

import java.util.Scanner;

/*
 * Write a program that asks the user for the lengths of the sides of a rectangle. Then print
 * - The area and perimeter of the rectangle
 * - The length of the diagonal (use the Pythagorean theorem)
 * Напишите программу, которая запрашивает у пользователя длину сторон прямоугольника. Затем распечатывать
 * - Площадь и периметр прямоугольника
 * - Длина диагонали (используйте теорему Пифагора)
 */
public class P2_08 {
    public static void main(String[] args) {
        System.out.println("Введите длину сторон прямоугольника:");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("Площадь прямоугольника " + (a * b));
        System.out.println("Периметр прямоугольника " + 2 * (a + b));
        double c = Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
        System.out.printf("Длина диагонали (используйте теорему Пифагора) %.2f" , c);
    }
}