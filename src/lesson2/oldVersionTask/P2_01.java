package lesson2.oldVersionTask;

/**
 * Write a program that displays the dimensions of a letter-size (width × height inches) sheet of paper in millimeters. There are 25.4 millimeters per inch. Use con­stants and comments in your program.
 * Напишите программу, которая отображает размеры листа бумаги (ширина × высота) в миллиметрах. Есть 25,4 миллиметра на дюйм. Используйте константы и комментарии в вашей программе.
 */

class P2_01 {

    public static final float MILLIMETER_PER_INCH = 25.4f; // константа, показывающая сколько мм в 1 дюйме

    public static void main(String[] args) {
        float inchWidth = 8.27f;    // ширина в дюймах
        float inchHeight = 11.70f;  // высота в дюймах

        // TODO возможно придется переделывать...
        int millisWidth = (int) (inchWidth * MILLIMETER_PER_INCH);
        int millisHeight = (int) (inchHeight * MILLIMETER_PER_INCH);
        System.out.print("Размер бумаги в мм: " + millisWidth + " x " + millisHeight);
    }
}