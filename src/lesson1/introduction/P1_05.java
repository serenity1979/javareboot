package lesson1.introduction;
/*
Write a program that displays your name inside a box on the screen, like this: Dave Do your best to approximate lines with characters such as |- + .
*/

public class P1_05 {
    public static void main(String[] args) {

        System.out.println("+----+");
        System.out.println("|Anna|");
        System.out.println("+----+");
    }
}
