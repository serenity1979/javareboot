package lesson1.introduction;
/*
Write a program that prints the balance of an account after the first, second, and third year. The account has an initial balance of $1,000 and earns 5 percent interest per year.
*/

public class P1_04 {
    public static void main(String[] args) {
        final int COUNT_YEARS = 3;
        final double PERCENT = 0.05;
        double balance = 1000;

        for (int i = 1; i <= COUNT_YEARS; i++) {
            balance *= (1 + PERCENT);
            System.out.printf("Баланс %d года составляет %.3f$%n", i, balance);
        }
    }
}