package lesson1.introduction;
/*
Write a program that prints the sum of the first ten positive integers, 1 + 2 + ... + 10.
*/

public class P1_02 {
    public static void main(String[] args) {
        final int END_NUMBER = 10;
        int total = 0;

        for (int i = 1; i <= END_NUMBER; i++) {
            total += i;
        }

        System.out.println("Всего (сумма чисел): " + total);
    }
}