package lesson1.introduction;

/*
Write a program that prints the product of the first ten positive integers, 1 × 2 × ... × 10. (Use * to indicate multiplication in Java.)
*/
public class P1_03 {
    public static void main(String[] args) {
        final int END_NUMBER = 10;
        int total = 1;

        for (int i = 1; i <= END_NUMBER; i++) {
            total *= i;
        }

        System.out.println("Всего (произведение чисел): " + total);
    }
}
